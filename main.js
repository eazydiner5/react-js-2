// JavaScript source code
import React, { Component } from "react";
import {
    Route,
    NavLink,
    HashRouter
} from "react-router-dom";
import Home from "./Users";
import Create from "./Users/Create";
import View from "./Users/View";
class Main extends Component {
    render() {
        return (
            <HashRouter>
        <div>
          <h1>Simple React App</h1>
                    <ul className="header">
                        <li><NavLink to="/Users">Home</NavLink></li>
                        <li><NavLink to="/Users/Create">Create User</NavLink></li>
                        <li><NavLink to="/Users/View">View User</NavLink></li>
            <li><a href="/Users">Home</a></li>
            <li><a href="/Users/Create">Stuff</a></li>
            <li><a href="/Users/View">Contact</a></li>
          </ul>
                    <div className="content">
                        <Route exact path="/Users" component={Home} />
                        <Route path="/Users/Create" component={Create} />
                        <Route path="/Users/View" component={View} />
             
          </div>
        </div>
      </HashRouter>
        );
    }
}

export default Main;