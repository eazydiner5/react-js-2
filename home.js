// JavaScript source code
import React, { Component } from "react";

class Home extends Component {
    render() {
        return (
            <div>
                <h2>HELLO</h2>
                <p>This Is A React App where you can create a user store it in a local storage and then view the users by retriving the data stored.</p>
            </div>
        );
    }
}

export default Home;