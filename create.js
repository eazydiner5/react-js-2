import React, { Component } from "react";
import { FormErrors } from './FormErrors';
import './Form.css';

class create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            gender: '',
            email: '',
            mobile: '',
            technology: [
                { id: 1, value: "C", isChecked: false },
                { id: 2, value: "C++", isChecked: false },
                { id: 3, value: "Java", isChecked: false },
                { id: 4, value: "Python", isChecked: false },
                { id: 5, value: "JavaScript", isChecked: false }
            ],
            profilepic: '',
            rememberMe: false,
            formErrors: { name: '', gender: '', email: '', mobile: '', technology: '', profilepic: '' },
            nameValid: false,
            genderValid: false,
            emailValid: false,
            mobileValid: false,
            technologyValid: false,
            profilepicValid: false,
            formValid: false
        }
    }
    handleChange = (event) => {
        const input = event.target;
        const value = input.type === 'checkbox' ? input.checked : input.value;

        this.setState({ [input.name]: value });
    };

    handleFormSubmit = () => { };

    render() { /*...*/ }

    handleFormSubmit = () => {
        const { user, rememberMe } = this.state;
        localStorage.setItem('rememberMe', rememberMe);
        localStorage.setItem('name', rememberMe ? name : '');
        localStorage.setItem('gender', rememberMe ? gender : '');
        localStorage.setItem('email', rememberMe ? email : '');
        localStorage.setItem('mobile', rememberMe ? mobile : '');
        localStorage.setItem('technology', rememberMe ? technology : '');
        localStorage.setItem('profilepic', rememberMe ? profilepic: '');
    };

    componentDidMount() {
        const rememberMe = localStorage.getItem('rememberMe') === 'true';
        const name = rememberMe ? localStorage.getItem('name') : '';
        const gender = rememberMe ? localStorage.getItem('gender') : '';
        const email = rememberMe ? localStorage.getItem('email') : '';
        const mobile = rememberMe ? localStorage.getItem('mobile') : '';
        const technology = rememberMe ? localStorage.getItem('technology') : '';
        const profilepic = rememberMe ? localStorage.getItem('profilepic') : '';
        this.setState({ user, rememberMe });
    }

    handleUserInput = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value },
            () => { this.validateField(name, value) });
    }

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let nameValid = this.state.nameValid;
        let genderValid = this.state.genderValid;
        let emailValid = this.state.emailValid;
        let mobileValid = this.state.mobileValid;
        let technologyValid = this.state.technologyValid;
        let profilepicValid = this.state.profilepicValid;

        switch (fieldName) {
            case 'name':
                emailValid = value.match(/^[a - zA - Z][a - zA - Z]{ 2, }$/i);
                fieldValidationErrors.name = nameValid ? '' : ' Please Enter Correct User Name';
                break;
            case 'email':
                emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                fieldValidationErrors.email = emailValid ? '' : ' Enter Valid Email Address';
                break;
            case 'mobile':
                mobileValid = value.match(/^[ 0-9 ]$/i);
                fieldValidationErrors.mobile = mobileValid ? '' : ' Enter Valid Mobile Number';
                break;
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            nameValid: nameValid,
            emailValid: emailValid,
            mobileValid: mobileValid
        }, this.validateForm);
    }

    validateForm() {
        this.setState({ formValid: this.state.nameValid && this.state.emailValid && this.state.mobileValid });
    }

    errorClass(error) {
        return (error.length === 0 ? '' : 'has-error');
    }

    handleAllChecked = (event) => {
        let technology = this.state.technology
        technology.forEach(technology => technology.isChecked = event.target.checked)
        this.setState({ technology: technology })
    }

    handleCheckChieldElement = (event) => {
        let technology = this.state.technology
        technology.forEach(technology => {
            if (technology.value === event.target.value)
                technology.isChecked = event.target.checked
        })
        this.setState({ technology: technology })
    }

    render() {
        return (
            <form className="React App">
                <h2>Create User</h2>
                <div className={`form-group ${this.errorClass(this.state.formErrors.name)}`}>
                    <label htmlFor="name">User Name</label>
                    <input type="name" required className="form-control" name="name"
                        placeholder="Name"
                        value={this.state.name}
                        onChange={this.handleUserInput} />
                     Name: <input name="name" value={this.state.name} onChange={this.handleChange} />
                </div>
                <form onSubmit={this.onSubmit}>
                    <strong>Gender</strong>
                    <ul>
                        <li>
                            <label>
                                <input
                                    type="radio"
                                    value="Male"
                                    checked={this.state.color === "red"}
                                    onChange={this.onRadioChange}
                                />
                                <span>Male</span>
                            </label>
                        </li>
                        <li>
                            <label>
                                <input
                                    type="radio"
                                    value="Female"
                                    checked={this.state.color === "red"}
                                    onChange={this.onRadioChange}
                                />
                                <span>Female</span>
                            </label>
                        </li>
                        <li>
                            <label>
                                <input
                                    type="radio"
                                    value="Others"
                                    checked={this.state.color === "red"}
                                    onChange={this.onRadioChange}
                                />
                                <span>Others</span>
                            </label>
                        </li>
                    </ul>
                </form>
                 Gender: <input name="gender" value={this.state.gender} onChange={this.handleChange} />
                <div className="panel panel-default">
                    <FormErrors formErrors={this.state.formErrors} />
                </div>
                <div className={`form-group ${this.errorClass(this.state.formErrors.email)}`}>
                    <label htmlFor="email">Email address</label>
                    <input type="email" required className="form-control" name="email"
                        placeholder="Email"
                        value={this.state.email}
                        onChange={this.handleUserInput} />
                </div>
                 Email Address: <input name="email" value={this.state.email} onChange={this.handleChange} />
                <div className={`form-group ${this.errorClass(this.state.formErrors.mobile)}`}>
                    <label htmlFor="mobile">Mobile Number</label>
                    <input type="mobile" required className="form-control" name="mobile"
                            placeholder="Mobile"
                        value={this.state.mobile}
                        onChange={this.handleUserInput} />
                </div>
                <h1> Check and Uncheck All Technology </h1>
                <input type="checkbox" onClick={this.handleAllChecked} value="checkedall" /> Check / Uncheck All
                <ul>
                    {
                        this.state.technology.map((technology) => {
                            return (<CheckBox handleCheckChieldElement={this.handleCheckChieldElement}  {...technology} />)
                        })
                    }
                </ul>
                 Technology: <input name="technology" value={this.state.technology onChange={this.handleChange} />
                <label>
                    <input name="rememberMe" checked={this.state.rememberMe} onChange={this.handleChange} type="checkbox" /> Remember me
      </label>
                <button type="submit" className="btn btn-primary" disabled={!this.state.formValid}>Create</button>
            </form>
        )
    }
}

export default Form;

